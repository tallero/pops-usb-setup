#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""POP USB drive setup utility"""

#    pops-usb-setup
#
#    ----------------------------------------------------------------------
#    Copyright © 2021  Pellegrino Prevete
#
#    All rights reserved
#    ----------------------------------------------------------------------
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from argparse import ArgumentParser
from shutil import copyfile
from os import getcwd, makedirs
from os.path import dirname, realpath
from os.path import join as path_join


def build(game,
          vcd,
          output_dir=None,
          video_mode="pal",
          source="archive",
          opl=True,
          uppercase=True,
          cheats=""):
    """Setup a directory to copy on usb to use it with POPS emulator

    Args:
        game (str): game identifier (what you want but no whitespaces);
        vcd (str): path of the vcd image;
        output_dir (str): root of the USB (or a directory);
        video_mode (str): can be "pal" and copy the 'patch no.8';
        opl (bool): whether to install Open PS2 Loader;
        source (str): can be 'archive' or 'bitbucket';
        uppercase (bool): if to use uppercase tags for games.
    """

    project_dir = dirname(realpath(__file__))
    config_dir = path_join(project_dir, "config")
    patch_dir = path_join(project_dir, "patches")

    if not output_dir:
        output_dir = path_join(getcwd(), 'build')

    print(f"Video mode: {video_mode}")
    print(f"Build directory: {output_dir}")

    build_dir = path_join(output_dir, f"build_{source}")
    pops_dir = path_join(build_dir, "POPS")
    opl_dir = path_join(build_dir, 'OPL')
    opl_path = path_join(project_dir, 'opl/OPNPS2LD.ELF')
    opl_output = path_join(opl_dir, "OPNPS2LD.ELF")

    if uppercase:
        game = game.upper()
    print(f"Game ID: {game}")

    game_patch_dir = path_join(pops_dir, game)

    iox_pak = f"usb/{source}/POPS_IOX.PAK"
    iox_pak_path = path_join(project_dir, iox_pak)

    if source == "bitbucket":
        popstarter = "usb/bitbucket/POPS.ELF"

    if source == "archive":
        popstarter = "usb/archive/POPSTARTER.ELF"

    popstarter_path = path_join(project_dir, popstarter)

    game_output = path_join(pops_dir, f'{game}.VCD')
    emulator_output = path_join(pops_dir, 'POPS_IOX.PAK')
    launcher_output = path_join(pops_dir, f'XX.{game}.ELF')

    try:
        makedirs(game_patch_dir)
    except FileExistsError:
        pass

    try:
        copyfile(vcd, game_output)
    except FileNotFoundError as not_found:
        print(not_found)
        print(f"ERROR: virtual cd file {game} not found")

    if not cheats:
        cheats = path_join(config_dir, "CHEATS.TXT")

    cheats_output = path_join(game_patch_dir, "CHEATS.TXT")
    copyfile(cheats, cheats_output)

    if video_mode == "pal":
        video_patch = path_join(patch_dir, "PATCH_8.BIN")
        video_patch_output = path_join(game_patch_dir, "PATCH_8.BIN")

    if video_mode == "ntsc":
        video_patch = path_join(patch_dir, "PATCH_9.BIN")
        video_patch_output = path_join(game_patch_dir, "PATCH_9.BIN")

    copyfile(video_patch, video_patch_output)

    copyfile(iox_pak_path, emulator_output)
    copyfile(popstarter_path, launcher_output)

    if opl:
        try:
            makedirs(opl_dir)
        except FileExistsError:
            pass

        copyfile(opl_path, opl_output)


def main():
    """ Main entry point"""
    parser = ArgumentParser(description="POPS USB Drive setup")

    game = {"args": ['game'],
            "kwargs": {'nargs': 1,
                       'action': 'store',
                       'help': 'output game directory game'}}

    vcd = {"args": ['vcd'],
           "kwargs": {'nargs': 1,
                      'action': 'store',
                      'help': 'vcd of the game'}}

    output_dir = {'args': ['--output-dir'],
                  'kwargs': {'nargs': 1,
                             'action': 'store',
                             'default': [""],
                             'help': 'output directory'}}

    all_regions = {"args": ['--all-regions'],
                   "kwargs": {'action': 'store_true',
                              'default': True,
                              'help': 'try all possible combinations'}}

    cheats = {"args": ['--cheats'],
              "kwargs": {'nargs': 1,
                         'action': 'store',
                         'default': [""],
                         'help': ('whether to copy over a cheat file '
                                  '(default: template from program source)')}}

    video_mode = {"args": ['--video-mode'],
                  "kwargs": {'nargs': 1,
                             'action': 'store',
                             'default': ['pal'],
                             'help': "can be 'pal' or 'ntsc'"}}

    opl = {"args": ['--opl'],
           "kwargs": {'action': 'store_true',
                      'default': True,
                      'help': 'whether to install OpenPS2 Loader'}}



    parser.add_argument(*game['args'], **game['kwargs'])
    parser.add_argument(*vcd['args'], **vcd['kwargs'])
    parser.add_argument(*output_dir['args'], **output_dir['kwargs'])
    parser.add_argument(*all_regions['args'], **all_regions['kwargs'])
    parser.add_argument(*video_mode['args'], **video_mode['kwargs'])
    parser.add_argument(*cheats['args'], **cheats['kwargs'])
    parser.add_argument(*opl['args'], **opl['kwargs'])

    args = parser.parse_args()

    build_args = (args.game[0],
                  args.vcd[0])
    build_kwargs = {"output_dir": args.output_dir[0],
                    "video_mode": args.video_mode[0],
                    "cheats": args.cheats[0],
                    "opl": args.opl}

    if all_regions:

        build_kwargs['source'] = 'archive'
        build(*build_args,
              **build_kwargs)

        build_kwargs['source'] = 'bitbucket'
        build(*build_args,
              **build_kwargs)
    else:
        build(*build_args, **build_kwargs)


if __name__ == "__main__":
    main()
