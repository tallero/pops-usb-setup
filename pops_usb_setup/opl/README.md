# Open PS2 Loader

Put the `elf` file for Open PS2 Loader (`OPNPS2LD.ELF`) inside this directory.

You can get OPL from 
[here](https://github.com/ps2homebrew/Open-PS2-Loader/releases).
