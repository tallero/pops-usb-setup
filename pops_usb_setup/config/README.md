# Cheats

Other than cheats this file contains configurations for the game.

Meaningful options are

- `$XPOS_XXX` (default: `640`): horizontal position;
- `$YPOS_00` (no default): vertical position;
- `$FORCEPAL`: force PAL mode;
- `$COMPATIBILITY_0x04`: can speed up emulation (if not slow usb);
- `CODECACHE_ADDON_0`: if previous didn't work.

Complete information available 
[here](https://bitbucket.org/ShaolinAssassin/popstarter-documentation-stuff/wiki/special-cheats).
