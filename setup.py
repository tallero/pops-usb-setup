#!/usr/bin/env python
"""Setup file for the module"""

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup_args = {
    "name": 'pops-usb-setup',
    "version": '0.0.1',
    "description": 'Setup an USB drive for the POPS emulator',
    "long_description": long_description,
    "long_description_content_type": "text/markdown",
    "author": 'Pellegrino Prevete',
    "author_email": 'pellegrinoprevete@gmail.com',
    "url": 'https://github.com/tallero/pops-usb-setup',
    "packages": find_packages(),
    "entry_points": {
      'console_scripts': ['pops-usb-setup = pops_usb_setup:main']
    },
    "include_package_data": True,
    "keywords": ['pops', 'psx'],
    "license": 'AGPLv3',
    "classifiers": [
      "Development Status :: 3 - Alpha",
      ("License :: OSI Approved :: GNU Affero General Public License v3 "
       "or later (GPLv3+)"),
      "Operating System :: Unix",
      "Topic :: System :: Software Distribution",
    ]
}

setup(**setup_args)
