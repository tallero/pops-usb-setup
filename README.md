# POPS USB Setup

An utility to setup PlayStation VCDs on USB drives for the 
[POPS emulators](https://emulation.gametechwiki.com/index.php/Official_Sony_Emulators), using
[POPStarter](https://web.archive.org/web/20211028182111/https://bitbucket.org/ShaolinAssassin/popstarter-documentation-stuff/wiki/usb-mode).


The only remaining piece to complete the toolchain is 
[`cue2pops`](https://github.com/tallero/cue2pops-linux') (not working).

# Install

Fill the `usb` directories as instructed in the READMEs, 
then install with:

```console
python3 setup.py install --user
```

Otherwise there is an AUR [package](https://aur.archlinux.org/packages/pops-usb-setup), 
which will fill the directories on its own when building.

# Usage

```console
    pops-usb-setup --help
```

# Troubleshooting

## Known working setup:

- `SCPH-50004` (`FMCB v1.966`, `ELFLauncher`): `source`: `archive`, `video_mode`;

## Games

### Crash Bandicoot 2

**MD5SUM**

```
81ddfa9001b3399451c4a004d0c43188
```

Needs `--video-mode="pal"` or

```
CHEATS.TXT
---------------
$YPOS_50
$FORCEPAL
```

# License

The project is released (where applicable) under AGPLv3.

Of course this project is intended to people willing to play their
back ups on their broken drive consoles.
